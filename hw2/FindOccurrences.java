import java.io.File;
import java.io.FileNotFoundException;
import java.util.Random;
import java.util.Scanner;

public class FindOccurrences {
	
	public static void main(String[] args) {
		int [][] matrix = readMatrix();
		int zero = 0;
		int one = 0;
		int two = 0;
		int three = 0;
		int four = 0;
		int five = 0;
		int six = 0;
		int seven = 0;
		int eight = 0;
		int nine = 0;

		for (int i = 0; i <matrix.length; i ++){
			for (int j = 0; j <matrix.length; j ++){
				if (matrix[i][j] == 0){
					zero+=1;}
				if (matrix[i][j] == 1){
					one+=1;}	
				if (matrix[i][j] == 2){
					two+=1;}
				if (matrix[i][j] == 3){
					three+=1;}
				if (matrix[i][j] == 4){
					four+=1;}
				if (matrix[i][j] == 5){
					five+=1;}
				if (matrix[i][j] == 6){
					six+=1;}
				if (matrix[i][j] == 7){
					seven+=1;}
				if (matrix[i][j] == 8){
					eight+=1;}
				if (matrix[i][j] == 9){
					nine+=1;}
				
		}}
	
		System.out.println("0 Occurrences " + zero + " times");
		System.out.println("1 Occurrences " + one + " times");
		System.out.println("2 Occurrences " + two + " times");
		System.out.println("3 Occurrences " + three + " times");
		System.out.println("4 Occurrences " + four + " times");
		System.out.println("5 Occurrences " + five + " times");
		System.out.println("6 Occurrences " + six + " times");
		System.out.println("7 Occurrences " + seven + " times");
		System.out.println("8 Occurrences " + eight + " times");
		System.out.println("9 Occurrences " + nine + " times");
		
		//print the occurrences 
	}


		
	private static int[][] readMatrix(){
		int[][] matrix = new int [10][10];
		File file = new File("matrix.txt");

	    try {

	        Scanner sc = new Scanner(file);
	        int i = 0;
	        int j = 0;
	        while (sc.hasNextLine()) {
	            int number = sc.nextInt();
	            matrix[i][j] = number;
	            if (j == 9)
	            	i++;
	            j = (j + 1) % 10;
	            if (i==10)
	            	break;
	        }
	        sc.close();
	    } 
	    catch (FileNotFoundException e) {
	        e.printStackTrace();
	    }		
		return matrix;
	}
	
}
