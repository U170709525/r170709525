public class MyDate {

	int day;
	int month;
	int year;
	int dayLimit;

	int[] thirtyOneDays = { 1, 3, 5, 7, 8, 10, 12 };
	int[] thirtyDays = { 4, 6, 9, 11 };

	public MyDate(int day, int month, int year) {

		this.day = day;
		this.month = month;
		this.year = year;

	}

	public void controlDay(int i) {

		for (int j = 0; j < thirtyDays.length; j++) {
			if (i == j) {

				dayLimit = 30;

				if (day > dayLimit) {

					day = dayLimit;

				}

			}
		}

		for (int j2 = 0; j2 < thirtyOneDays.length; j2++) {

			if (i == j2) {
				dayLimit = 31;
				if (day > dayLimit) {
					day = dayLimit;
				}

			}
		}

		if (i == 2) {

			if (year % 4 == 0) {
				dayLimit = 29;

				if (day > dayLimit) {

					day = dayLimit;
				}
			} else {
				dayLimit = 28;
				if (day < dayLimit) {
					day = day % dayLimit;
				}

			}
		}
	}

	public String toString() {

		if (day < 10 && month < 10) {
			return year + "- " + "0" + month + "- " + "0"
					+ ((day > 29 && month == 2) ? 29 : day);
		} else if (month < 10) {
			return year + "- " + "0" + month + "- "
					+ ((day > 29 && month == 2) ? 29 : day);
		}

		else if (day < 10) {
			return year + "- " + month + "- " + "0"
					+ ((day > 29 && month == 2) ? 29 : day);

		}

		else {
			return year + "- " + month + "- "
					+ ((day > 29 && month == 2) ? 29 : day);
		}

	}

	public void incrementDay() {

		controlDay(month);
		if (++day >= dayLimit) {

			incrementMonth();
			day = day % dayLimit;

		}
	}

	public void incrementYear(int i) {

		for (int j = 0; j < i; j++) {

			incrementYear();

		}

	}

	public void decrementDay() {
		if (--day <= 0) {

			controlDay(--month);

			day = dayLimit;

		}
	}

	public void decrementYear() {

		if (year % 4 == 0 && month == 2 && day == 29) {
			decrementDay();

		}

		year -= 1;

	}

	public void decrementMonth() {

		controlDay(month);

		if (--month == 0) {
			decrementYear();
			month = 12;
		}

	}

	public void incrementDay(int i) {

		controlDay(month);
		day += i;

	}

	public void decrementMonth(int i) {

		for (int j = 0; j < i; j++) {

			decrementMonth();

		}

	}

	public void decrementYear(int i) {

		for (int j = 0; j < i; j++) {

			decrementYear();

		}

	}

	public void incrementMonth() {

		controlDay(month);

		if (month == 13) {

			incrementYear();
			month = 1;

		}

		month += 1;

	}

	public void incrementYear() {

		controlDay(month);

		year += 1;

	}

	public void decrementDay(int i) {

		for (int j = 0; j < i; j++) {

			decrementDay();

		}

	}

	public void incrementMonth(int i) {

		for (int j = 0; j < i; j++) {

			incrementMonth();

		}

	}

	public boolean isBefore(MyDate anotherDate) {

		String dateString = Integer.toString(year) + Integer.toString(month)
				+ Integer.toString(day);
		String anotherDateString = Integer.toString(anotherDate.year)
				+ Integer.toString(anotherDate.month)
				+ Integer.toString(anotherDate.day);

		if (Integer.parseInt(dateString) < Integer.parseInt(anotherDateString)) {
			return true;
		} else {
			return false;
		}

	}

	public boolean isAfter(MyDate anotherDate) {

		String dateString = Integer.toString(year) + Integer.toString(month)
				+ Integer.toString(day);
		String anotherDateString = Integer.toString(anotherDate.year)
				+ Integer.toString(anotherDate.month)
				+ Integer.toString(anotherDate.day);

		if (Integer.parseInt(dateString) > Integer.parseInt(anotherDateString)) {
			return true;
		} else {
			return false;
		}

	}

	public int dayDifference(MyDate anotherDate) {

		int result = 0;

		while (anotherDate.year != year || anotherDate.month != month
				|| anotherDate.day != day) {

			anotherDate.incrementDay();
			result++;

		}

		return result;
	}

}
